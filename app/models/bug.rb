class Bug < ActiveRecord::Base
  validates :description, presence: true,
                          length: {minimum: 10}
  validates :url, presence: true,
                  length: {minimum: 5}
  
end
