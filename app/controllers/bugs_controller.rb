class BugsController < ApplicationController
  def index
    # @bugs = Bug.all
    @bugs = Bug.last(5)
  end

  def show
    @bug = Bug.find(params[:id])
  end

  def new
    @bugs = Bug.last(5)
    @bug = Bug.new
  end

  def create
    @bug = Bug.new(bug_params)

    if @bug.save
      flash[:success] = "Bug #{@bug.id} został utworzony!\nDziękujemy za zgłoszenie :)"
      redirect_to new_bug_path
    else
      flash[:error] = "Coś nie halo. Popraw podświetlone pola."
      render :new
    end
  end

  def edit
    @bug = Bug.find(params[:id])
  end

  def update
    @bug = Bug.find(params[:id])

    if @bug.update_attributes(bug_params)
      flash[:success] = "Edycja zakończona powodzeniem."
      redirect_to new_bug_path
    else
      flash[:error] = "Coś poszło nie tak. Spróbuj ponownie."
      render :edit
    end
  end

  def destroy
    @bug = Bug.find(params[:id]).destroy
    redirect_to bugs_path
    flash[:notice] = "Bug #{@bug.id} został usunięty!"
  end

  private

    def bug_params
      params.require(:bug).permit(:description, :url)
    end 

end