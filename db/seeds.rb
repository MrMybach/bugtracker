# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Bug.create(description: 'Nie działają linki', url: '/linki/url')
Bug.create(description: 'Strna się źle wyświetla', url: '/strona/sie/zle/wyswietla')
Bug.create(description: 'Banner jest do góry nogami', url: '/do/gory/nogami')
Bug.create(description: 'Czekolada jest za słona', url: '/za/slona')
Bug.create(description: 'Nie mam przeglądarki', url: '/brak/browsera')
