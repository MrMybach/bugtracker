class CreateBugs < ActiveRecord::Migration
  def change
    create_table :bugs do |t|
      t.text :description
      t.string :url

      t.timestamps
    end
  end
end
